﻿/*
 * Code adapted from:
 * http://www.albahari.com/nutshell/predicatebuilder.aspx
 * 2007-2012, Joe Albahari, Ben Albahari and O'Reilly Media, Inc. All rights reserved
 */

using System;
using System.Linq;
using System.Linq.Expressions;

namespace ObjectSpecifications
{
    public static class ExpressionExtensions
    {
        public static Expression<Func<T, bool>> Or<T>(
            this Expression<Func<T, bool>> expr1,
            Expression<Func<T, bool>> expr2)
        {
            var invokedExpr = Expression.Invoke(expr2, expr1.Parameters.Cast<Expression>());
            return Expression.Lambda<Func<T, bool>>(
                Expression.OrElse(expr1.Body, invokedExpr),
                expr1.Parameters);
        }

        public static Expression<Func<T, bool>> And<T>(
            this Expression<Func<T, bool>> expr1,
            Expression<Func<T, bool>> expr2)
        {
            var invokedExpr = Expression.Invoke(expr2, expr1.Parameters.Cast<Expression>());
            return Expression.Lambda<Func<T, bool>>(
                Expression.AndAlso(expr1.Body, invokedExpr),
                expr1.Parameters);
        }

        public static Expression<Func<T, bool>> Not<T>(this Expression<Func<T, bool>> expression)
        {
            var invokedExpr = Expression.Invoke(expression, expression.Parameters.Cast<Expression>());
            return Expression.Lambda<Func<T, bool>>(
                Expression.Not(invokedExpr),
                expression.Parameters);
        }
    }
}