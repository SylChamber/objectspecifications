﻿using System;

namespace ObjectSpecifications
{
    public static class StringExtensions
    {
        public static void CheckAgainstNull(this string str, string parameterName = "")
        {
            if (string.IsNullOrEmpty(str))
            {
                throw new ArgumentNullException(parameterName);
            }
        }
    }
}
