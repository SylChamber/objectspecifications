﻿using System;

namespace ObjectSpecifications
{
    public static class ObjectExtensions
    {
        public static void CheckAgainstNull(this object obj, string parameterName = "")
        {
            if (obj == null)
            {
                throw new ArgumentNullException(parameterName);
            }
        }
    }
}
