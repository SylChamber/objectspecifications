﻿namespace ObjectSpecifications
{
    public static class SpecificationExtensions
    {
        public static Specification<T> And<T>(
            this Specification<T> leftSpec,
            Specification<T> rightSpec)
        {
            var newSpecExp = leftSpec.IsSatisfiedBy()
                .And(rightSpec.IsSatisfiedBy());

            return new Specification<T>(newSpecExp);
        }

        public static Specification<T> Or<T>(
            this Specification<T> leftSpec,
            Specification<T> rightSpec)
        {
            var newSpecExp = leftSpec.IsSatisfiedBy()
                .Or(rightSpec.IsSatisfiedBy());

            return new Specification<T>(newSpecExp);
        }

        public static Specification<T> Not<T>(this Specification<T> spec)
        {
            var newSpecExp = spec.IsSatisfiedBy().Not();

            return new Specification<T>(newSpecExp);
        }
    }
}
