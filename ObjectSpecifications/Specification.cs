﻿using System;
using System.Linq.Expressions;

namespace ObjectSpecifications
{
    public class Specification<T>
    {
        public Specification(Expression<Func<T, bool>> matchExpression)
        {
            matchExpression.CheckAgainstNull("matchExpression");
            this.matchExpression = matchExpression;
        }

        private readonly Expression<Func<T, bool>> matchExpression;

        public Expression<Func<T, bool>> IsSatisfiedBy()
        {
            return matchExpression;
        }

        public static implicit operator Func<T, bool>(Specification<T> spec)
        {
            return spec.IsSatisfiedBy().Compile();
        }

        public static implicit operator Expression<Func<T, bool>>(Specification<T> spec)
        {
            return spec.IsSatisfiedBy();
        }

        public static Specification<T> operator &(
            Specification<T> leftSpec,
            Specification<T> rightSpec)
        {
            return leftSpec.And(rightSpec);
        }

        public static Specification<T> operator |(
            Specification<T> leftSpec,
            Specification<T> rightSpec)
        {
            return leftSpec.Or(rightSpec);
        }

        public static Specification<T> operator !(Specification<T> spec)
        {
            return spec.Not();
        }
    }
}
