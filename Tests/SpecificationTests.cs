﻿using FluentAssertions;
using ObjectSpecifications;
using System;
using System.Linq.Expressions;
using Xunit;
using Xunit.Extensions;

namespace Tests
{
    [Trait("Specification", "")]
    public class SpecificationTests
    {
        [Theory(DisplayName = "correcty satisfies specification")]
        [InlineData(6, 5, false)]
        [InlineData(4, 5, true)]
        public void CorrectlySatisfiesSpecification(
            int comparedValue, int referenceValue, bool expectedValue)
        {
            var smallerThanSpec = SpecForSmallerThan(referenceValue);
            var smallerThan = smallerThanSpec.IsSatisfiedBy().Compile();

            smallerThan(comparedValue).Should().Be(expectedValue);
        }

        private static Specification<int> SpecForSmallerThan(int value)
        {
            Expression<Func<int, bool>> smallerThanExp = i => i < value;
            var smallerThanSpec = new Specification<int>(smallerThanExp);
            return smallerThanSpec;
        }

        [Theory(DisplayName = "is implicitly converted to func delegate")]
        [InlineData(11, 10, true)]
        [InlineData(9, 10, false)]
        public void IsImplicitlyConvertedToFunc(
            int comparedValue, int referenceValue, bool expectedValue)
        {
            var greaterThanSpec = SpecForGreaterThan(referenceValue);
            Func<int, bool> greaterThan = greaterThanSpec;

            greaterThan(comparedValue).Should().Be(expectedValue);
        }

        private Specification<int> SpecForGreaterThan(int value)
        {
            Expression<Func<int, bool>> greaterThanExp = i => i > value;
            var greaterThanSpec = new Specification<int>(greaterThanExp);
            return greaterThanSpec;
        }

        [Theory(DisplayName = "is implicitly converted to lambda expression")]
        [InlineData(11, 10, true)]
        [InlineData(9, 10, false)]
        public void IsImplicitlyConvertedToExpression(
            int comparedValue, int referenceValue, bool expectedValue)
        {
            var greaterThanSpec = SpecForGreaterThan(referenceValue);
            Expression<Func<int, bool>> greaterThanExpression = greaterThanSpec;
            var greaterThan = greaterThanExpression.Compile();
            
            greaterThan(comparedValue).Should().Be(expectedValue);
        }

        [Theory(DisplayName = "combines with another specification via And")]
        [InlineData(4, 5, 10, false)]
        [InlineData(6, 5, 10, true)]
        [InlineData(11, 5, 10, false)]
        public void CombinesWithOtherSpecificationViaAnd(
            int comparedValue, int minLimit, int maxLimit, bool expectedValue)
        {
            var greaterThanSpec = SpecForGreaterThan(minLimit);
            var smallerThanSpec = SpecForSmallerThan(maxLimit);

            var betweenMinAndMaxSpec = greaterThanSpec.And(smallerThanSpec);
            var betweenMinAndMax = betweenMinAndMaxSpec.IsSatisfiedBy().Compile();

            betweenMinAndMax(comparedValue).Should().Be(expectedValue);
        }

        [Theory(DisplayName = "supports the & operator")]
        [InlineData(4, 5, 10, false)]
        [InlineData(6, 5, 10, true)]
        [InlineData(11, 5, 10, false)]
        public void SupportsAndOperator(
            int comparedValue, int minLimit, int maxLimit, bool expectedValue)
        {
            var greaterThanSpec = SpecForGreaterThan(minLimit);
            var smallerThanSpec = SpecForSmallerThan(maxLimit);

            var betweenMinAndMaxSpec = greaterThanSpec & smallerThanSpec;
            var betweenMinAndMax = betweenMinAndMaxSpec.IsSatisfiedBy().Compile();

            betweenMinAndMax(comparedValue).Should().Be(expectedValue);
        }

        [Theory(DisplayName = "combines with another specification via Or")]
        [InlineData(4, 5, 10, true)]
        [InlineData(6, 5, 10, false)]
        [InlineData(11, 5, 10, true)]
        public void CombinesWithOtherSpecificationViaOr(
            int comparedValue, int minLimit, int maxLimit, bool expectedValue)
        {
            var smallerThanMinSpec = SpecForSmallerThan(minLimit);
            var greaterThanMaxSpec = SpecForGreaterThan(maxLimit);

            var belowMinOrAboveMaxSpec = smallerThanMinSpec.Or(greaterThanMaxSpec);
            var belowMinOrAboveMax = belowMinOrAboveMaxSpec.IsSatisfiedBy().Compile();

            belowMinOrAboveMax(comparedValue).Should().Be(expectedValue);
        }

        [Theory(DisplayName = "supports the | operator")]
        [InlineData(4, 5, 10, true)]
        [InlineData(6, 5, 10, false)]
        [InlineData(11, 5, 10, true)]
        public void SupportsOrOperator(
            int comparedValue, int minLimit, int maxLimit, bool expectedValue)
        {
            var smallerThanMinSpec = SpecForSmallerThan(minLimit);
            var greaterThanMaxSpec = SpecForGreaterThan(maxLimit);

            var belowMinOrAboveMaxSpec = smallerThanMinSpec | greaterThanMaxSpec;
            var belowMinOrAboveMax = belowMinOrAboveMaxSpec.IsSatisfiedBy().Compile();

            belowMinOrAboveMax(comparedValue).Should().Be(expectedValue);
        }

        [Theory(DisplayName = "is negated via Not")]
        [InlineData(5, 5, true)]
        [InlineData(4, 5, false)]
        [InlineData(6, 5, true)]
        public void IsNegatedViaNot(
            int comparedValue, int referenceValue, bool expectedValue)
        {
            var smallerThanSpec = SpecForSmallerThan(referenceValue);

            var greaterOrEqualToSpec = smallerThanSpec.Not();
            var greaterOrEqualTo = greaterOrEqualToSpec.IsSatisfiedBy().Compile();

            greaterOrEqualTo(comparedValue).Should().Be(expectedValue);
        }

        [Theory(DisplayName = "supports the ! operator")]
        [InlineData(5, 5, true)]
        [InlineData(4, 5, false)]
        [InlineData(6, 5, true)]
        public void SupportsNotOperator(
            int comparedValue, int referenceValue, bool expectedValue)
        {
            var smallerThanSpec = SpecForSmallerThan(referenceValue);

            var greaterOrEqualToSpec = !smallerThanSpec;
            var greaterOrEqualTo = greaterOrEqualToSpec.IsSatisfiedBy().Compile();

            greaterOrEqualTo(comparedValue).Should().Be(expectedValue);
        }

    }
}
